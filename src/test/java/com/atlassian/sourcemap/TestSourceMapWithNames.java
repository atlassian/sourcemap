package com.atlassian.sourcemap;

import java.io.IOException;

import org.junit.Test;
import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import com.google.gson.Gson;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import static com.atlassian.sourcemap.ReadableSourceMap.toWritableSourceMap;
import static com.atlassian.sourcemap.ReadableSourceMapImpl.fromSource;

public class TestSourceMapWithNames {

    private final Gson gson = new Gson();

    @Test
    public void shouldParseSourceMap() throws IOException {
        String mapAsString = Resources.toString(Resources.getResource("test.map"), Charsets.UTF_8);

        final WritableSourceMap check = toWritableSourceMap(fromSource(mapAsString));

        // parse json and convert to string again to ignore formatting differences
        String generate = check.generate();
        assertThat(
                gson.toJson(gson.fromJson(check.generate(), SourceMapJson.class)),
                equalTo(gson.toJson(SourceMapJson.parse(mapAsString))));
    }
}
