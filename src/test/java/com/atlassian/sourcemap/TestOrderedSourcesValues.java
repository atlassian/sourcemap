package com.atlassian.sourcemap;

import org.junit.Test;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

public class TestOrderedSourcesValues {

    @Test
    public void shouldAddItemToListAndMap() {
        OrderedSourcesValues orderedSourcesValues = new OrderedSourcesValues();
        orderedSourcesValues.add("foo");

        assertThat(orderedSourcesValues.getValues(), equalTo(singletonList("foo")));
        assertThat(orderedSourcesValues.getIndex("foo"), equalTo(0));
    }

    @Test
    public void callingAddWithTheSameStringValueMultipleTimesShouldNotAddThemAgain() {
        OrderedSourcesValues orderedSourcesValues = new OrderedSourcesValues();
        orderedSourcesValues.add("foo");
        orderedSourcesValues.add("foo");
        orderedSourcesValues.add("foo");

        assertThat(orderedSourcesValues.getValues().size(), equalTo(1));
        assertThat(orderedSourcesValues.getValues(), equalTo(singletonList("foo")));
        assertThat(orderedSourcesValues.getIndex("foo"), equalTo(0));
    }

    @Test
    public void callingAddWithNullMultipleTimesShouldContinueToAddThem() {
        OrderedSourcesValues orderedSourcesValues = new OrderedSourcesValues();
        orderedSourcesValues.add(null);
        orderedSourcesValues.add(null);
        orderedSourcesValues.add(null);

        assertThat(orderedSourcesValues.getValues().size(), equalTo(3));
        assertThat(orderedSourcesValues.getValues(), equalTo(asList(null, null, null)));
    }

    @Test
    public void replaceingShouldRemovePreviousValueButKeepPointingTheIndex() {
        OrderedSourcesValues orderedSourcesValues = new OrderedSourcesValues();
        orderedSourcesValues.add("foo");
        orderedSourcesValues.add("bar");

        assertThat(orderedSourcesValues.getValues(), hasItem("foo"));
        assertThat(orderedSourcesValues.getIndex("foo"), equalTo(0));

        // now lets replace "foo"
        orderedSourcesValues.replaceAt(0, "baz");

        assertThat(orderedSourcesValues.getValues(), not(hasItem("foo")));
        assertThat(orderedSourcesValues.getValues(), equalTo(asList("baz", "bar")));
        assertThat(orderedSourcesValues.getIndex("foo"), equalTo(0));
        assertThat(orderedSourcesValues.getIndex("baz"), equalTo(0));
    }
}
