package com.atlassian.sourcemap;

import org.junit.Test;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class TestSourceMapJson {

    @Test
    public void shouldFillSourceContentsIfNotSet() {
        String sourcemapJson =
                "{ \"version\": 3, \"sources\": [\"foo.js\", \"bar.js\", \"baz.js\"], \"mappings\": \"\", \"file\": \"js/main.js\" }";
        SourceMapJson sourcemapBean = SourceMapJson.parse(sourcemapJson);
        assertThat(sourcemapBean.getSourcesContent(), equalTo(asList(null, null, null)));
    }

    @Test
    public void shouldFillSourceContentsIfNotPopulatedProperly() {
        String sourcemapJson =
                "{ \"version\": 3, \"sources\": [\"foo.js\", \"bar.js\", \"baz.js\"], \"mappings\": \"\", \"file\": \"js/main.js\", \"sourcesContent\": [,\"1;\"] }";
        SourceMapJson sourcemapBean = SourceMapJson.parse(sourcemapJson);
        assertThat(sourcemapBean.getSourcesContent(), equalTo(asList(null, null, null)));
    }
}
