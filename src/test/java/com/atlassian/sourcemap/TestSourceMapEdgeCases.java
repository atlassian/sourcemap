package com.atlassian.sourcemap;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Test;
import com.google.common.io.Resources;

import static com.google.common.base.Charsets.UTF_8;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

import static com.atlassian.sourcemap.ReadableSourceMapImpl.fromSource;

public class TestSourceMapEdgeCases {

    private String loadSourceMap(final String path) throws IOException {
        return Resources.toString(Resources.getResource(path), UTF_8);
    }

    private int getMappingLineCount(ReadableSourceMap sourceMap) {
        final AtomicInteger count = new AtomicInteger();

        sourceMap.eachMapping(mapping -> {
            count.getAndIncrement();
        });

        return count.intValue();
    }

    @Test
    public void shouldCorrectlyJoinSourcemapsWithAndWithoutSourcesContent() throws IOException {
        String mapA = loadSourceMap("sourcemaps-with-and-without-sourcesContents-case#1/sourcemap-with-content.js.map");
        String mapB =
                loadSourceMap("sourcemaps-with-and-without-sourcesContents-case#1/sourcemap-without-content.js.map");

        ReadableSourceMap sourceMapA = fromSource(mapA);
        ReadableSourceMap sourceMapB = fromSource(mapB);
        SourceMapJoiner sourceMapJoiner = new SourceMapJoiner();

        // add "a"
        sourceMapJoiner.add(sourceMapA, getMappingLineCount(sourceMapA));
        // add "b"
        sourceMapJoiner.add(sourceMapB, getMappingLineCount(sourceMapB));

        // turn the joined sourcemap into a readable sourcemap
        ReadableSourceMap joinedSourceMap = fromSource(sourceMapJoiner.join().generate());

        // expect all sources of "A" and "B" to be within the joined sourcemap
        assertThat(
                joinedSourceMap.getSources(), hasItems(sourceMapA.getSources().toArray(new String[0])));
        assertThat(
                joinedSourceMap.getSources(), hasItems(sourceMapB.getSources().toArray(new String[0])));

        // expect all sourcesContents of "A" and "B" to be within the joined sourcemap
        assertThat(
                joinedSourceMap.getSourcesContent(),
                hasItems(sourceMapA.getSourcesContent().toArray(new String[0])));
        assertThat(
                joinedSourceMap.getSourcesContent(),
                hasItems(sourceMapB.getSourcesContent().toArray(new String[0])));
    }

    @Test
    public void shouldCorrectlyUpdateTheNamesOfSourcesIfTheyClashWithDivergingSourceContent() throws IOException {
        String mapA =
                loadSourceMap("sourcemaps-with-clashing-sources-and-no-sourcesContent-names-case#3/sourcemap-a.js.map");
        String mapB =
                loadSourceMap("sourcemaps-with-clashing-sources-and-no-sourcesContent-names-case#3/sourcemap-b.js.map");

        ReadableSourceMap sourceMapA = fromSource(mapA);
        ReadableSourceMap sourceMapB = fromSource(mapB);
        SourceMapJoiner sourceMapJoiner = new SourceMapJoiner();

        // add "a"
        sourceMapJoiner.add(sourceMapA, getMappingLineCount(sourceMapA));
        // add "b"
        sourceMapJoiner.add(sourceMapB, getMappingLineCount(sourceMapB));

        // turn the joined sourcemap into a readable sourcemap
        ReadableSourceMap joinedSourceMap = fromSource(sourceMapJoiner.join().generate());

        // replicate the "uniqification"
        List<String> sources = sourceMapA.getSources();
        List<String> renamedSources = sourceMapA.getSources().stream()
                .map(name -> name + "-uniquified-1")
                .collect(toList());
        sources.addAll(renamedSources);
        assertThat(joinedSourceMap.getSources(), equalTo(sources));

        // expect all sourcesContents of "A" and "B" to be within the joined sourcemap as they are all null
        assertThat(
                joinedSourceMap.getSourcesContent(),
                hasItems(sourceMapA.getSourcesContent().toArray(new String[0])));
        assertThat(
                joinedSourceMap.getSourcesContent(),
                hasItems(sourceMapB.getSourcesContent().toArray(new String[0])));
    }

    @Test
    public void shouldMergeConflictingSourcesWithAndWithoutSourceContentsCorrectly() throws IOException {
        String mapA = loadSourceMap(
                "sourcemaps-with-clashing-sources-and-with-and-without-sourcesContent-case#3/sourcemap-with-content.js.map");
        String mapB = loadSourceMap(
                "sourcemaps-with-clashing-sources-and-with-and-without-sourcesContent-case#3/sourcemap-without-content.js.map");

        ReadableSourceMap sourceMapA = fromSource(mapA);
        ReadableSourceMap sourceMapB = fromSource(mapB);
        SourceMapJoiner sourceMapJoiner = new SourceMapJoiner();

        // add "a"
        sourceMapJoiner.add(sourceMapA, getMappingLineCount(sourceMapA));
        // add "b"
        sourceMapJoiner.add(sourceMapB, getMappingLineCount(sourceMapB));

        // turn the joined sourcemap into a readable sourcemap
        ReadableSourceMap joinedSourceMap = fromSource(sourceMapJoiner.join().generate());

        // replicate the "uniqification"
        List<String> sources = sourceMapA.getSources();
        List<String> renamedSources = sourceMapA.getSources().stream()
                .map(name -> name + "-uniquified-1")
                .collect(toList());
        sources.addAll(renamedSources);
        assertThat(joinedSourceMap.getSources(), equalTo(sources));

        // expect all sourcesContents of "A" and "B" to be within the joined sourcemap as they are all null
        List<String> sourceContents = sourceMapA.getSourcesContent();
        sourceContents.addAll(sourceMapB.getSourcesContent());
        assertThat(joinedSourceMap.getSourcesContent(), equalTo(sourceContents));
    }

    @Test
    public void shouldChangeNamesOfClashingSources() throws IOException {
        String mapA = loadSourceMap("sourcemaps-with-clashing-sources-case#3/sourcemap-a.js.map");
        String mapB = loadSourceMap("sourcemaps-with-clashing-sources-case#3/sourcemap-b.js.map");

        ReadableSourceMap sourceMapA = fromSource(mapA);
        ReadableSourceMap sourceMapB = fromSource(mapB);
        SourceMapJoiner sourceMapJoiner = new SourceMapJoiner();

        // add "a"
        sourceMapJoiner.add(sourceMapA, getMappingLineCount(sourceMapA));
        // add "b"
        sourceMapJoiner.add(sourceMapB, getMappingLineCount(sourceMapB));

        // turn the joined sourcemap into a readable sourcemap
        ReadableSourceMap joinedSourceMap = fromSource(sourceMapJoiner.join().generate());

        // replicate the "uniqification"
        List<String> sources = sourceMapA.getSources();
        List<String> renamedSources = sourceMapA.getSources().stream()
                .map(name -> name + "-uniquified-1")
                .collect(toList());
        sources.addAll(renamedSources);
        assertThat(joinedSourceMap.getSources(), equalTo(sources));

        // expect all sourcesContents of "A" and "B" to be within the joined sourcemap as they are all null
        List<String> sourceContents = sourceMapA.getSourcesContent();
        sourceContents.addAll(sourceMapB.getSourcesContent());
        assertThat(joinedSourceMap.getSourcesContent(), equalTo(sourceContents));
    }

    @Test
    public void shouldDeduplicateSourcesAndSourceContentsIfTheyAreTheSameAfterResolvingSourceRoot() throws IOException {
        final String base = "sourcemaps-with-sourceRoot-and-clashing-absolute-names-but-same-sourceContents-case#3/";
        String mapA = loadSourceMap(base + "sourcemap-with-root.js.map");
        String mapB = loadSourceMap(base + "sourcemap-without-root.js.map");

        ReadableSourceMap sourceMapA = fromSource(mapA);
        ReadableSourceMap sourceMapB = fromSource(mapB);
        SourceMapJoiner sourceMapJoiner = new SourceMapJoiner();

        // add "a"
        sourceMapJoiner.add(sourceMapA, getMappingLineCount(sourceMapA));
        // add "b"
        sourceMapJoiner.add(sourceMapB, getMappingLineCount(sourceMapB));

        // turn the joined sourcemap into a readable sourcemap
        ReadableSourceMap joinedSourceMap = fromSource(sourceMapJoiner.join().generate());

        // Expect "A" and "B" and "joined" to be the same
        assertThat(joinedSourceMap.getSources(), equalTo(sourceMapA.getSources()));
        assertThat(joinedSourceMap.getSources(), equalTo(sourceMapB.getSources()));
        assertThat(sourceMapA.getSources(), equalTo(sourceMapB.getSources()));

        assertThat(joinedSourceMap.getSourcesContent(), equalTo(sourceMapA.getSourcesContent()));
        assertThat(joinedSourceMap.getSourcesContent(), equalTo(sourceMapB.getSourcesContent()));
        assertThat(sourceMapA.getSourcesContent(), equalTo(sourceMapB.getSourcesContent()));
    }

    @Test
    public void shouldResolveSourceRootAndPotentiallyAvoidClashes() throws IOException {
        String mapA = loadSourceMap(
                "sourcemaps-with-clashing-sources-but-one-with-sourceRoot-case#1/sourcemap-with-root.js.map");
        String mapB = loadSourceMap(
                "sourcemaps-with-clashing-sources-but-one-with-sourceRoot-case#1/sourcemap-without-root.js.map");

        ReadableSourceMap sourceMapA = fromSource(mapA);
        ReadableSourceMap sourceMapB = fromSource(mapB);
        SourceMapJoiner sourceMapJoiner = new SourceMapJoiner();

        // add "a"
        sourceMapJoiner.add(sourceMapA, getMappingLineCount(sourceMapA));
        // add "b"
        sourceMapJoiner.add(sourceMapB, getMappingLineCount(sourceMapB));

        // turn the joined sourcemap into a readable sourcemap
        ReadableSourceMap joinedSourceMap = fromSource(sourceMapJoiner.join().generate());

        // Expect "A" to not clash with "B" and therefore "joined" to be the collection of both
        List<String> sources = sourceMapA.getSources();
        sources.addAll(sourceMapB.getSources());
        assertThat(joinedSourceMap.getSources(), equalTo(sources));

        // as there is no overlap in sourcesContent expect the same as with sources
        List<String> sourcesContent = sourceMapA.getSourcesContent();
        sourcesContent.addAll(sourceMapB.getSourcesContent());
        assertThat(joinedSourceMap.getSourcesContent(), equalTo(sourcesContent));
    }

    @Test
    public void joinedSourecmapShouldReuseExistingSourcesContents() throws IOException {
        String mapA = loadSourceMap("sourcemaps-with-the-same-sourcesContent-case#2#4/sourcemap-a.js.map");
        String mapB = loadSourceMap("sourcemaps-with-the-same-sourcesContent-case#2#4/sourcemap-b.js.map");

        ReadableSourceMap sourceMapA = fromSource(mapA);
        ReadableSourceMap sourceMapB = fromSource(mapB);
        SourceMapJoiner sourceMapJoiner = new SourceMapJoiner();

        // add "a"
        sourceMapJoiner.add(sourceMapA, getMappingLineCount(sourceMapA));
        // take a snapshot before adding "b"
        ReadableSourceMap joinedAOnly = fromSource(sourceMapJoiner.join().generate());
        // add "b"
        sourceMapJoiner.add(sourceMapB, getMappingLineCount(sourceMapB));
        // parse json and convert to string again to ignore formatting differences
        ReadableSourceMap joinedSourceMap = fromSource(sourceMapJoiner.join().generate());

        // the sources of A and B are the same, therefore they also match those of the joined map
        assertThat(sourceMapA.getSourcesContent(), equalTo(sourceMapB.getSourcesContent()));
        assertThat(joinedSourceMap.getSourcesContent(), equalTo(sourceMapB.getSourcesContent()));
        assertThat(joinedSourceMap.getSourcesContent(), equalTo(sourceMapA.getSourcesContent()));

        // If we only had added "A" the sources would match those of "a"
        assertThat(joinedAOnly.getSources(), equalTo(sourceMapA.getSources()));
        // but as we add "B" they will be overwritten with the sources of B as the sourceContents are the same
        assertThat(joinedSourceMap.getSources(), equalTo(sourceMapB.getSources()));
        assertThat(joinedSourceMap.getSources(), not(equalTo(sourceMapA.getSources())));
    }

    @Test
    public void joinedSourecmapShouldReuseExistingSourcesContentsButNotNull() throws IOException {
        String mapA = loadSourceMap("sourcemaps-with-the-same-sourcesContent-and-null-case#2#4/sourcemap-a.js.map");
        String mapB = loadSourceMap("sourcemaps-with-the-same-sourcesContent-and-null-case#2#4/sourcemap-b.js.map");

        ReadableSourceMap sourceMapA = fromSource(mapA);
        ReadableSourceMap sourceMapB = fromSource(mapB);
        SourceMapJoiner sourceMapJoiner = new SourceMapJoiner();

        // add "a"
        sourceMapJoiner.add(sourceMapA, getMappingLineCount(sourceMapA));
        // add "b"
        sourceMapJoiner.add(sourceMapB, getMappingLineCount(sourceMapB));

        // turn the joined sourcemap into a readable sourcemap
        ReadableSourceMap joinedSourceMap = fromSource(sourceMapJoiner.join().generate());

        // expect all sources of "A" to be replaced with "B" except the one that has a sourceContent of `null`
        assertThat(joinedSourceMap.getSources(), hasItem("module1-webpack:///./made-up-file.js"));
        assertThat(
                joinedSourceMap.getSources(),
                not(
                        hasItem(
                                "module1-webpack:///external {\"var\":\"require('@atlassian/clientside-extensions-registry')\",\"amd\":\"@atlassian/clientside-extensions-registry\"}")));
        assertThat(joinedSourceMap.getSources(), not(hasItem("module1-webpack:///./src/main/frontend/unix-epoch.js")));
        assertThat(
                joinedSourceMap.getSources(),
                not(hasItem("module1-webpack:///./generated-clientside-extension/unix-epoch-js.js")));

        // expect all sources of "B" to exist
        assertThat(joinedSourceMap.getSources(), hasItem("module2-webpack:///./made-up-file.js"));
        assertThat(
                joinedSourceMap.getSources(),
                hasItem(
                        "module2-webpack:///external {\"var\":\"require('@atlassian/clientside-extensions-registry')\",\"amd\":\"@atlassian/clientside-extensions-registry\"}"));
        assertThat(joinedSourceMap.getSources(), hasItem("module2-webpack:///./src/main/frontend/unix-epoch.js"));
        assertThat(
                joinedSourceMap.getSources(),
                hasItem("module2-webpack:///./generated-clientside-extension/unix-epoch-js.js"));

        // expect the sourcesContent to be the same as "a" or "b" with an additional `null`
        List<String> sourceAWithNull = sourceMapA.getSourcesContent();
        sourceAWithNull.add(null);
        List<String> sourceBWithNull = sourceMapB.getSourcesContent();
        sourceBWithNull.add(null);

        assertThat(sourceBWithNull, equalTo(sourceAWithNull));
        assertThat(joinedSourceMap.getSourcesContent(), equalTo(sourceAWithNull));
        assertThat(joinedSourceMap.getSourcesContent(), equalTo(sourceBWithNull));
    }
}
