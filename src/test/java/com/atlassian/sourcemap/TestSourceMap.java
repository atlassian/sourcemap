package com.atlassian.sourcemap;

import org.junit.Test;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import static com.atlassian.sourcemap.ReadableSourceMap.toWritableSourceMap;
import static com.atlassian.sourcemap.ReadableSourceMapImpl.fromSource;
import static com.atlassian.sourcemap.Util.countLines;
import static com.atlassian.sourcemap.WritableSourceMap.toReadableSourceMap;

public class TestSourceMap {
    @Test
    public void shouldGenerateSourceMap() {
        WritableSourceMap map = new WritableSourceMapImpl.Builder()
                .withSources(singletonList("/a.js"))
                .build();
        map.addMapping(0, 0, 0, 0, "/a.js");
        map.addMapping(0, 5, 0, 10, "/a.js");
        map.addMapping(2, 0, 4, 0, "/a.js");
        map.addMapping(2, 5, 9, 0, "/a.js");
        assertThat(map.generate(), containsString("AAAA,KAAU;;AAIV,KAKA"));
    }

    @Test
    public void shouldParseSourceMap() {
        WritableSourceMap map = new WritableSourceMapImpl.Builder()
                .withSources(singletonList("/a.js"))
                .build();
        map.addMapping(0, 0, 0, 0, "/a.js");
        map.addMapping(0, 5, 0, 10, "/a.js");
        map.addMapping(2, 0, 4, 0, "/a.js");
        map.addMapping(2, 5, 9, 0, "/a.js");
        String mapAsString = map.generate();

        final WritableSourceMap check = toWritableSourceMap(fromSource(mapAsString));

        assertThat(check.generate(), equalTo(mapAsString));
    }

    @Test
    public void shouldGenerate1to1SourceMap() {
        assertThat(
                Util.create1to1SourceMap("var a = 1;\nvar b = 2", "/script.js").generate(),
                containsString("\"mappings\":\"AAAA;AACA\""));
    }

    @Test
    public void shouldOffsetSourceMap() {
        WritableSourceMap map = new WritableSourceMapImpl.Builder()
                .withSources(singletonList("/a.js"))
                .build();
        map.addMapping(0, 0, 0, 0, "/a.js");
        map.addMapping(1, 0, 1, 0, "/a.js");
        map.addMapping(2, 0, 4, 0, "/a.js");
        String originalSourceMap = map.generate();

        ReadableSourceMap mapWithOffset = toReadableSourceMap(map);
        mapWithOffset.addOffset(3);
        mapWithOffset.addOffset(1);

        WritableSourceMap expected = new WritableSourceMapImpl.Builder()
                .withSources(singletonList("/a.js"))
                .build();
        expected.addMapping(4, 0, 0, 0, "/a.js");
        expected.addMapping(5, 0, 1, 0, "/a.js");
        expected.addMapping(6, 0, 4, 0, "/a.js");

        assertThat(toWritableSourceMap(mapWithOffset).generate(), equalTo(expected.generate()));
        assertThat(map.generate(), equalTo(originalSourceMap));
    }

    @Test
    public void shouldJoinMaps() {
        String src1 = "var a = 1;\nvar b = 2";
        String src2 = "var a = 1;\nvar b = 2";
        WritableSourceMap map1 = Util.create1to1SourceMap(src1, "/script1.js");
        WritableSourceMap map2 = Util.create1to1SourceMap(src2, "/script2.js");
        String src1WithOffsets = "\n\n" + src1 + "\n\n";
        String src2WithOffsets = "\n\n" + src2 + "\n\n";

        assertThat(
                countLines(src1WithOffsets) + countLines(src2WithOffsets),
                equalTo(countLines(src1WithOffsets + "\n" + src2WithOffsets)));

        SourceMapJoiner joiner = Util.joiner();
        joiner.add(toReadableSourceMap(map1), countLines(src1WithOffsets), 2);
        joiner.add(toReadableSourceMap(map2), countLines(src2WithOffsets), 2);

        SourceMapGenerator expected = new SourceMapGenerator();
        expected.addSourceAndContents(asList("/script1.js", "/script2.js"), asList(null, null));
        expected.addMapping(2, 0, 0, 0, "/script1.js");
        expected.addMapping(3, 0, 1, 0, "/script1.js");
        expected.addMapping(8, 0, 0, 0, "/script2.js");
        expected.addMapping(9, 0, 1, 0, "/script2.js");
        assertThat(joiner.join().generate(), equalTo(expected.generate()));
    }

    @Test
    public void shouldRebaseMaps() {
        WritableSourceMap base = new WritableSourceMapImpl.Builder()
                .withSources(singletonList("/a.js"))
                .build();
        // Switching first and second lines, leaving third line intact (a, b, c -> b, a, c)
        base.addMapping(0, 0, 1, 0, "/a.js");
        base.addMapping(1, 0, 0, 0, "/a.js");
        base.addMapping(2, 0, 2, 0, "/a.js");

        WritableSourceMap map = new WritableSourceMapImpl.Builder()
                .withSources(singletonList("/a.js"))
                .build();
        // Switching second and third lines (b, a, c -> b, c, a).
        map.addMapping(0, 0, 0, 0, "/a.js");
        map.addMapping(1, 0, 2, 0, "/a.js");
        map.addMapping(2, 0, 1, 0, "/a.js");

        // Final transformations should be (a, b, c -> b, c, a)
        WritableSourceMap expected = new WritableSourceMapImpl.Builder()
                .withSources(singletonList("/a.js"))
                .build();
        expected.addMapping(0, 0, 1, 0, "/a.js");
        expected.addMapping(1, 0, 2, 0, "/a.js");
        expected.addMapping(2, 0, 0, 0, "/a.js");

        WritableSourceMap rebased = Util.rebase(toReadableSourceMap(map), toReadableSourceMap(base));
        assertThat(rebased.generate(), equalTo(expected.generate()));

        // Also make sure rebase works for map in read state.
        WritableSourceMap rebased2 = Util.rebase(toReadableSourceMap(map), toReadableSourceMap(base));
        assertThat(rebased2.generate(), equalTo(expected.generate()));
    }

    @Test
    public void shouldRebaseIfSomeMappingsAreMissing() {
        WritableSourceMap base = new WritableSourceMapImpl.Builder()
                .withSources(singletonList("/a.js"))
                .build();
        // Shifting source, there will be no first line in the mapping (a, b -> none, a, b)
        base.addMapping(1, 0, 0, 0, "/a.js");
        base.addMapping(2, 0, 1, 0, "/a.js");

        WritableSourceMap map = new WritableSourceMapImpl.Builder()
                .withSources(singletonList("/a.js"))
                .build();
        // Shifting one more time (none, a, b -> none, none, a, b).
        map.addMapping(1, 0, 0, 0, "/a.js");
        map.addMapping(2, 0, 1, 0, "/a.js");
        map.addMapping(3, 0, 2, 0, "/a.js");

        // Final transformations should be (a, b -> none, none, a, b)
        WritableSourceMap expected = new WritableSourceMapImpl.Builder()
                .withSources(singletonList("/a.js"))
                .build();
        expected.addMapping(2, 0, 0, 0, "/a.js");
        expected.addMapping(3, 0, 1, 0, "/a.js");

        WritableSourceMap rebased = Util.rebase(toReadableSourceMap(map), toReadableSourceMap(base));
        assertThat(rebased.generate(), equalTo(expected.generate()));
    }

    @Test
    public void shouldGenerateAfterRead() {
        WritableSourceMap writableSourceMap = new WritableSourceMapImpl.Builder()
                .withSources(singletonList("/a.js"))
                .build();
        writableSourceMap.addMapping(1, 0, 0, 0, "/a.js");
        String sourceMapContent = writableSourceMap.generate();
        ReadableSourceMap readableSourceMap = fromSource(sourceMapContent);
        assertThat(
                sourceMapContent, equalTo(toWritableSourceMap(readableSourceMap).generate()));
    }

    @Test
    public void shouldGenerateEmptyMap() {
        assertThat(
                (new WritableSourceMapImpl.Builder().empty().build()).generate(),
                equalTo("{\"version\":3,\"sources\":[],\"sourcesContent\":[],\"names\":[],\"mappings\":\"\"}"));
    }
}
